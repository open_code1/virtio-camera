# VirtIO-Camera

## About

Virtio-camera project is a VirtIO camera device protocol specification and its implementation.

## Status

| Component     | Status                    |
|---------------|---------------------------|
| Protocol      | In progress, will change  |
| Kernel driver | In progress, works        |
| Qemu          | In progress, works        |
| Crosvm        | Not started               |

## Protocol

Virtio-camera protocol specification is in early stage. For now it is almost directly based on V4L2 Linux driver UAPI. For simplicity, VirtIO format IDs are directly mapped to the V4L2 UAPI formats. Protocol may change significantly in the future.

Documentation is unavailable. Refer to the source code for the protocol definition.

## Building

### Guest kernel

Link: https://gitlab.collabora.com/dmitry.osipenko/linux-kernel-rd/-/tree/virtio-camera

Make sure that `CONFIG_VIDEO_VIRTIO_CAMERA` option is enabled in your kernel config.

Example:

```bash
git clone https://gitlab.collabora.com/dmitry.osipenko/linux-kernel-rd.git
cd linux-kernel-rd
git checkout virtio-camera
make defconfig && make -j5
```

### Qemu

Link: https://gitlab.collabora.com/dmitry.osipenko/qemu/-/tree/virtio-camera

Make sure that `libv4l-devel` packages installed in your system.

Example:

```bash
git clone https://gitlab.collabora.com/dmitry.osipenko/qemu.git
cd qemu
git checkout virtio-camera
mkdir build
cd build
sh ../configure --target-list=x86_64-softmmu --disable-werror
ninja
```

Make sure that this line presents the meson configuration log:

```
libv4l2 support              : YES
```

## Running

Virtio-camera of Qemu supports V4L2 cameras only. This example assumes that V4L2 camera device is located at `/dev/video0`. If your camera is located at a different path, then alter the path accordingly.

Preparation of the guest disk image and Qemu setup are outside the scope of this guide.

Example:

```bash
cd qemu/build
./x86_64-softmmu/qemu-system-x86_64 \
 -M microvm \
 -enable-kvm \
 -cpu host \
 -m 4G \
 -kernel linux-kernel-rd/arch/x86_64/boot/bzImage \
 -append "console=ttyS0" \
 -nodefaults \
 -no-user-config \
 -serial mon:stdio \
 -netdev user,id=u1 \
 -device virtio-net-device,netdev=u1 \
 -device virtio-gpu-device \
 -device virtio-tablet-device \
 -device virtio-keyboard-device \
 -device virtio-camera-device,v4ldev=/dev/video0
```

The virtio-camera V4L2 device will be available in guest as `/dev/video0`.

